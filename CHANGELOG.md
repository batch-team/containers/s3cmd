# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [1.0.2] - 2021-07-28

### Added

- Add python-magic package 

## [1.0.1] - 2021-07-28

### Added

- Symlink for python -> python3

## [1.0.0] - 2021-07-23

### Added

- Imported version from https://gitlab.cern.ch/batch-team/infra/docker-images

### Changed

- Add Helm v3 patch from Ankur
