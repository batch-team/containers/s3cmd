# s3cmd

## Release process

The current version of the repository releases three versions:
* `test`: it is built on every commit to the `master` branch.
* `<tag>`: it is built on every tag.
* `latest`: it is built on every tag.

The standard release procedure is:
1. Create a merge request with your change. This will trigger a build that won't be pushed to the repository. This merge request should add the modifications to the _Unrealesed_ section.
2. Once merge, do the test/QA with the image tagged _test_.
3. Promote the changes in CHANGELOG.md to the new version and create a merge request.
4. Once merged, tag the merge commit with the release (semver). This should trigger a new build to build and push `<tag>` and `latest`.
